from random import randint

def calculate_monster_attack(attack_min, attack_max):
    return randint(attack_min, attack_max)

def game_ends(winner_name):
    print(f"{winner_name} wins!")

game_running = True
game_results = []

while game_running == True:
    counter = 0
    new_round = True

    player = {"name": "Chris", "attack" : 10, "heal": 16, "health": 100}
    monster = {"name": "Loki", "attack_min": 8, "attack_max": 17, "health": 100}

    print("___" * 7)
    print("Enter player name")
    player["name"] = input()

    print("___" * 7)
    print(player["name"] + " has " + str(player["health"]) + " health points")
    print(monster["name"] + " has " + str(monster["health"]) + " health points")

    while new_round == True:
        counter += 1
        player_won = False
        monster_won = False

        print("___" * 7)
        print("Please select action.")
        print("1) Attack")
        print("2) Heal")
        print("3) Exit Game")
        print("4) Print Results")

        player_choice = input()

        if player_choice == '1':
            monster["health"] -= player["attack"]
            if monster["health"] <= 0:
                player_won = True

            else:
                player["health"] -= calculate_monster_attack(monster["attack_min"], monster["attack_max"])
                if player["health"] <= 0:
                    monster_won = True

        elif player_choice == "2":
            player["health"] += player["heal"]

            player["health"] -= calculate_monster_attack(monster["attack_min"], monster["attack_max"])
            if player["health"] <= 0:
                    monster_won = True

        elif player_choice == "3":
            game_running = False
            new_round = False

        elif player_choice == "4":
            for result in game_results:
                print(result)
                
        else: 
            print("Invalid input")


        if player_won == False and monster_won == False:
            print(player["name"] + " has " + str(player["health"]) + " health point left")
            print(monster["name"] + " has " + str(monster["health"]) + " health point left")
        elif player_won:
            game_ends(player["name"])
            round_result = {"name": player["name"], "health": player["health"], "rounds": counter}
            game_results.append(round_result)

        elif monster_won:
            game_ends(monster["name"])
            round_result = {"name": monster["name"], "health": monster["health"], "rounds": counter}
            game_results.append(round_result)
            
        if player_won == True or monster_won == True:
            new_round = False

